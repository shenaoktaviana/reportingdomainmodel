﻿using DomainModelFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CulinaryBizDomainModel
{
    public static class CustomExtension
    {
        /// <summary>
        /// Attaches entity graph to context using entity id to determinate if entity is new or modified.
        /// If Id is zero then entity is treated as NEW and otherwise it is treated as modified.
        /// If we want to save more than just root entity than child types must be supplied.
        /// If entity in graph is not root nor of child type it will be attached but not saved
        /// (it will be treated as unchanged).
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="rootEntity">The root entity.</param>
        /// <param name="childTypes">The child types that should be saved with root entity.</param>
        public static void AttachByIdValue<TEntity>(this DbContext context, TEntity rootEntity, HashSet<Type> childTypes)
            where TEntity : BaseEntity
        {
            // mark root entity as added
            // this action adds whole graph and marks each entity in it as added
            context.Set<TEntity>().Add(rootEntity);
            // in case root entity has id value mark it as modified (otherwise it stays added)

            // Get primary key value (If you have more than one key column, this will need to be adjusted)
            var keyNames = rootEntity.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).ToList();

            long keyValue = (long)keyNames[0].GetValue(rootEntity, null);

            if (keyValue != 0)
            {
                context.Entry(rootEntity).State = EntityState.Modified;
            }
            // traverse all entities in context (hopefully they are all part of graph we just attached)
            foreach (var entry in context.ChangeTracker.Entries<BaseEntity>())
            {
                // we are only interested in graph we have just attached
                // and we know they are all marked as Added 
                // and we will ignore root entity because it is already resolved correctly
                if (entry.State == EntityState.Added && entry.Entity != rootEntity)
                {
                    // if no child types are defined for saving then just mark all entities as unchanged)
                    if (childTypes == null || childTypes.Count == 0)
                    {
                        entry.State = EntityState.Unchanged;
                    }
                    else
                    {
                        // request object type from context because we might got reference to dynamic proxy
                        // and we wouldn't want to handle Type of dynamic proxy
                        Type entityType = ObjectContext.GetObjectType(entry.Entity.GetType());

                        var entityKeyNames = entry.Entity.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).ToList();

                        var entityKeyValue = new object();

                        foreach (PropertyInfo entityKeyName in entityKeyNames)
                        {
                            entityKeyValue = entityKeyName.GetValue(entry.Entity, null);

                            if (entityKeyValue == Convert.ChangeType(0, entityKeyValue.GetType()))
                                break;
                        }

                        long createdByValue = entry.Entity.CreatedBy;

                        bool isNew = entityKeyValue.Equals(Convert.ChangeType(0, entityKeyValue.GetType()));

                        // if type is not child type than it should not be saved so mark it as unchanged
                        if (!childTypes.Contains(entityType))
                        {
                            entry.State = EntityState.Unchanged;
                        }
                        else if (!isNew)
                        //else if (entityKeyValue != Convert.ChangeType(0, entityKeyValue.GetType()))
                        {
                            // if entity should be saved with root entity
                            // than if it has id mark it as modified 
                            // else leave it marked as added

                            if (createdByValue == 0)
                            {
                                entry.State = EntityState.Deleted;
                            }
                            else
                            {
                                entry.State = EntityState.Modified;
                            }

                        }
                        else if (isNew)
                        {
                            // if entity should be saved with root entity
                            // than if it has id mark it as modified 
                            // else leave it marked as added
                            entry.State = EntityState.Added;
                        }
                    }
                }
            }
        }
    }
}
