﻿using ReportingResources;
using DomainModelFramework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ReportingDomainModel
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        internal class UserMetadata
        {
            [Display(ResourceType = typeof(FrameworkResources), Name = "Email")]
            [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "FieldInvalid", ErrorMessageResourceType = typeof(FrameworkResources))]
            [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
            public string Email { get; set; }

            [Display(ResourceType = typeof(FrameworkResources), Name = "MainPhone")]
            [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
            public string Phone { get; set; }

            [JsonIgnore]
            public virtual User ParentUser { get; set; }

            [JsonIgnore]
            public virtual ICollection<ProjectTaskMember> ProjectTaskMembers { get; set; }

            [JsonIgnore]
            public virtual ICollection<ProjectTaskMember> ProjectTaskMembers1 { get; set; }

            [JsonIgnore]
            public virtual ICollection<User> ChildUsers { get; set; }

            [JsonIgnore]
            public virtual ICollection<UserJobRole> UserJobRoles { get; set; }

            [JsonIgnore]
            public virtual ICollection<DayOffData> DayOffDatas { get; set; }

            [JsonIgnore]
            public virtual ICollection<DayOffData> DayOffDatas1 { get; set; }
        }

        public UserModulePrivilege GetUserModulePrivilege(long modulePrivilegeID)
        {
            UserModulePrivilege result = this.UserModulePrivileges.FirstOrDefault(x => x.ModulePrivilegeID == modulePrivilegeID);

            return result;
        }

        public string FullName
        {
            get
            {
                return this.FirstName + ((string.IsNullOrEmpty(this.LastName) ? string.Empty : " ")) + this.LastName;
            }
        }

        public string SupervisorNames
        {
            get
            {
                if (this.ParentUser != null)
                    return this.ParentUser.FirstName;

                return "-";
            }
        }

        public string RoleNames
        {
            get
            {
                if (this.UserJobRoles != null && this.UserJobRoles.Count(x => x.IsActive) > 0)
                {
                    string roleNames = string.Empty;

                    foreach (UserJobRole userJobRole in this.UserJobRoles.Where(x => x.IsActive))
                    {
                        if (!string.IsNullOrEmpty(roleNames))
                            roleNames += ", ";

                        roleNames += userJobRole.JobRole.Name;
                    }

                    return DMCommonUtilities.TrimString(roleNames, 50);
                }

                return "-";
            }
        }
    }
}
