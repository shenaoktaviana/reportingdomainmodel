﻿namespace ReportingDomainModel
{
    public class MultipleAutoCompleteModel
    {
        public long ModelID { get; set; }
        public string ModelName { get; set; }
        public long? ParentID { get; set; }
        public string ParentName { get; set; }
    }
}