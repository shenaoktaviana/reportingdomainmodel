﻿using DomainModelFramework;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReportingDomainModel
{
    [MetadataType(typeof(ProjectTaskMemberMetadata))]
    public partial class ProjectTaskMember
    {
        internal class ProjectTaskMemberMetadata
        {

        }

        public string AssigneeName { get; set; }

        public string DateWithDay
        {
            get
            {
                return this.Date.ToString("ddd, dd MMM yyyy");
            }
        }

        public string TotalTasksText
        {
            get
            {
                return this.ProjectTaskMemberDetails.Count().ToString();
            }
        }

        public string ReviewerName
        {
            get
            {
                if (this.ReviewerUser != null)
                    return this.ReviewerUser.FullName;

                return string.Empty;
            }
        }

        public string StatusFullDesc
        {
            get
            {
                string status = string.Empty;

                switch (this.Status)
                {
                    case "APPROVED":
                        status = GeneralResources.Approved;
                        break;
                    case "WAITINGFORREVIEW":
                        status = GeneralResources.WaitingForReview;
                        break;
                    case "REJECTED":
                        status = GeneralResources.Rejected;
                        break;
                }

                return status + (this.ReviewerUser != null ? (" (" + GeneralResources.By + " " + this.ReviewerUser.FullName + ")") : string.Empty);
            }
        }
    }
}
