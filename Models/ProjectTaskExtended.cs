﻿using DomainModelFramework;
using Newtonsoft.Json;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReportingDomainModel
{
    [MetadataType(typeof(ProjectTaskMetadata))]
    public partial class ProjectTask
    {
        internal class ProjectTaskMetadata
        {
            [Display(ResourceType = typeof(GeneralResources), Name = "Name")]
            [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
            public string Name { get; set; }

            [JsonIgnore]
            public virtual Project Project { get; set; }

            [JsonIgnore]
            public virtual ICollection<ProjectTask> ChildProjectTasks { get; set; }

            [JsonIgnore]
            public virtual ICollection<DailyScrumDetail> DailyScrumDetails { get; set; }

            [JsonIgnore]
            public virtual ICollection<ProjectTaskMemberDetail> ProjectTaskMemberDetails { get; set; }
        }
        
        public string ProjectName { get; set; }

        public string ParentName { get; set; }

        public string TrimmedParentProjectTaskName
        {
            get
            {
                if (this.ParentProjectTask != null)
                    return DMCommonUtilities.TrimString(this.ParentProjectTask.Name, 25);
                else
                    return "-";
            }
        }

        public string TrimmedName
        {
            get
            {
                return DMCommonUtilities.TrimString(this.Name, 30);
            }
        }

        public string ReportParentTaskName
        {
            get
            {
                return this.ParentProjectTask != null ? this.ParentProjectTask.Name.ToUpper() : (GeneralResources.NoGroup.ToUpper());
            }
        }

        public string GetTaskDescriptionsFromTaskMemberDetailInPeriod(DateTime? startDate, DateTime? endDate)
        {
            IEnumerable<ProjectTaskMemberDetail> taskDetails = this.ProjectTaskMemberDetails.Where(x => (startDate.HasValue ? x.ProjectTaskMember.Date >= startDate : true)
                && (endDate.HasValue ? x.ProjectTaskMember.Date <= endDate : true));

            string[] descriptionList = taskDetails.Where(x => x.Description != null).Select(x => x.Description).Distinct().ToArray();
            string mergedDescription = string.Empty;

            foreach (string desc in descriptionList)
            {
                if (!string.IsNullOrEmpty(mergedDescription))
                    mergedDescription += System.Environment.NewLine;

                mergedDescription += "- " + desc;
            }

            return mergedDescription;
        }

        public string TrimmedNameWithIsRoutine
        {
            get
            {
                return DMCommonUtilities.TrimString((this.IsRoutine ? "(Routine) " : string.Empty) + this.Name, 50);
            }
        }

        public string NameWithIsRoutine
        {
            get
            {
                return (this.IsRoutine ? "(Routine) " : string.Empty) + this.Name;
            }
        }

        public string AssigneeNames
        {
            get
            {
                string assigneeNames = string.Empty;

                foreach (IEnumerable<ProjectTaskMemberDetail> details in this.ProjectTaskMemberDetails.GroupBy(x => x.ProjectTaskMember.AssigneeID))
                {
                    ProjectTaskMemberDetail defaultDetail = details.FirstOrDefault();

                    if (!string.IsNullOrEmpty(assigneeNames))
                        assigneeNames += ", ";

                    assigneeNames += defaultDetail.ProjectTaskMember.User.FirstName;
                }

                return string.IsNullOrEmpty(assigneeNames) ? "-" : DMCommonUtilities.TrimString(assigneeNames, 25);
            }
        }

        public string GetAssigneeNamesInPeriod(DateTime? startDate, DateTime? endDate, bool isAllDesc = true, string description = null)
        {
            IEnumerable<ProjectTaskMemberDetail> taskDetails = this.ProjectTaskMemberDetails.Where(x => (startDate.HasValue ? x.ProjectTaskMember.Date >= startDate : true)
                && (endDate.HasValue ? x.ProjectTaskMember.Date <= endDate : true));

            if (!isAllDesc)
                taskDetails = taskDetails.Where(x => x.Description == description);

            string assigneeNames = string.Empty;

            foreach (IEnumerable<ProjectTaskMemberDetail> details in taskDetails.GroupBy(x => x.ProjectTaskMember.AssigneeID))
            {
                ProjectTaskMemberDetail defaultDetail = details.FirstOrDefault();

                if (!string.IsNullOrEmpty(assigneeNames))
                    assigneeNames += ", ";

                assigneeNames += defaultDetail.ProjectTaskMember.User.FirstName;
            }

            return string.IsNullOrEmpty(assigneeNames) ? "-" : DMCommonUtilities.TrimString(assigneeNames, 25);
        }

        public decimal GetSubtotalOfPeriod(DateTime? startDate, DateTime? endDate, bool isAllDesc = true, string description = null, long? assigneeID = null)
        {
            IEnumerable<ProjectTaskMemberDetail> taskDetails = this.ProjectTaskMemberDetails.Where(x => (startDate.HasValue ? x.ProjectTaskMember.Date >= startDate : true)
                && (endDate.HasValue ? x.ProjectTaskMember.Date <= endDate : true));

            if (!isAllDesc)
                taskDetails = taskDetails.Where(x => x.Description == description);

            if (assigneeID != null)
                taskDetails = taskDetails.Where(x => x.ProjectTaskMember.AssigneeID == assigneeID);

            if (taskDetails != null)
                return taskDetails.Sum(x => x.Duration);

            return 0;
        }

        public decimal GetSubtotalOfPreviousPeriod(DateTime? date, bool isAllDesc = true, string description = null, long? assigneeID = null)
        {
            if (date.HasValue)
            {
                IEnumerable<ProjectTaskMemberDetail> taskDetails = this.ProjectTaskMemberDetails.Where(x => x.ProjectTaskMember.Date < date);

                if (!isAllDesc)
                    taskDetails = taskDetails.Where(x => x.Description == description);

                if (assigneeID != null)
                    taskDetails = taskDetails.Where(x => x.ProjectTaskMember.AssigneeID == assigneeID);

                if (taskDetails != null)
                    return taskDetails.Sum(x => x.Duration);
            }

            return 0;
        }

        public string GetLatestStatusInPeriodByParentTaskID(DateTime? startDate, DateTime? endDate, bool isAllDesc = true, string description = null, long? assigneeID = null)
        {
            if (this.IsClosed && this.ClosedDate >= startDate)
                return GeneralResources.Closed.ToUpper();

            IEnumerable<ProjectTaskMemberDetail> taskDetails = this.ProjectTaskMemberDetails.Where(x => (startDate.HasValue ? x.ProjectTaskMember.Date >= startDate : true)
               && (endDate.HasValue ? x.ProjectTaskMember.Date <= endDate : true));

            if (!isAllDesc)
                taskDetails = taskDetails.Where(x => x.Description == description);

            if (assigneeID != null)
                taskDetails = taskDetails.Where(x => x.ProjectTaskMember.AssigneeID == assigneeID);

            if (taskDetails != null && taskDetails.Count() > 0)
                return taskDetails.OrderByDescending(x => x.ProjectTaskMember.Date).FirstOrDefault().StatusTask.Name.ToUpper();
            else
            {
                taskDetails = this.ProjectTaskMemberDetails.Where(x => x.ProjectTaskMember.Date < startDate);

                if (taskDetails != null && taskDetails.Count() > 0)
                    return taskDetails.OrderByDescending(x => x.ProjectTaskMember.Date).FirstOrDefault().StatusTask.Name.ToUpper();
            }

            return GeneralResources.NotStarted.ToUpper();
        }
    }
}
