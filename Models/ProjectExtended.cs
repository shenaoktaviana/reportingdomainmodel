﻿using DomainModelFramework;
using Newtonsoft.Json;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReportingDomainModel
{
    [MetadataType(typeof(ProjectMetadata))]
    public partial class Project
    {
        internal class ProjectMetadata
        {
            [JsonIgnore]
            public virtual ICollection<ProjectTask> ProjectTasks { get; set; }
        }

        public string TrimmedDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Description))
                {
                    return (DMCommonUtilities.TrimString(this.Description, 50));
                }
                else return "-";
            }
        }
    }
}
