﻿using DomainModelFramework;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReportingDomainModel
{
    [MetadataType(typeof(JobRoleMetadata))]
    public partial class JobRole
    {
        internal class JobRoleMetadata
        {
        }

        public string ParentJobRoleName
        {
            get
            {
                if (this.ParentJobRole != null)
                    return this.ParentJobRole.Name;

                return "-";
            }
        }

        public string TrimmedJobdesk
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Jobdesk))
                    return DMCommonUtilities.TrimString(this.Jobdesk, 30);

                return "-";
            }
        }

        public int TotalActiveUsers
        {
            get
            {
                if (this.UserJobRoles != null)
                    return this.UserJobRoles.Count(x => x.User.IsActive);

                return 0;
            }
        }

        public int TotalInactiveUsers
        {
            get
            {
                if (this.UserJobRoles != null)
                    return this.UserJobRoles.Count(x => !x.User.IsActive);

                return 0;
            }
        }
    }
}
