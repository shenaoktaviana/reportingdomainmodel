﻿using DomainModelFramework;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ReportingDomainModel
{
    [MetadataType(typeof(LanguageMetadata))]
    public partial class Language
    {
        internal class LanguageMetadata
        {
            [Display(ResourceType = typeof(FrameworkResources), Name = "Name")]
            [Required(ErrorMessage = null, ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(FrameworkResources))]
            public string Name { get; set; }
        }
    }
}
