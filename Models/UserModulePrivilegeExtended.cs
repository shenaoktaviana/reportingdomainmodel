﻿using DomainModelFramework;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ReportingDomainModel
{
    [MetadataType(typeof(UserModulePrivilegeMetadata))]
    public partial class UserModulePrivilege
    {
        internal class UserModulePrivilegeMetadata
        {
            [JsonIgnore]
            public virtual User User { get; set; }
            [JsonIgnore]
            public virtual ModulePrivilege ModulePrivilege { get; set; }
            [JsonIgnore]
            public virtual PrivilegeState PrivilegeState { get; set; }
        }

        public bool IsAllowed
        {
            get
            {
                bool result = false;

                if (this.PrivilegeState != null)
                {
                    if (this.PrivilegeState.Name == PrivilegeStateName.ALLOWED.ToString())
                    {
                        result = true;
                    }
                }

                return result;
            }
        }

        public bool IsStateAllowed { get; set; }
    }
}
