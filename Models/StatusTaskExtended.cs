﻿using DomainModelFramework;
using Newtonsoft.Json;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReportingDomainModel
{
    [MetadataType(typeof(StatusTaskMetadata))]
    public partial class StatusTask
    {
        internal class StatusTaskMetadata
        {
            [JsonIgnore]
            public virtual ICollection<ProjectTaskMemberDetail> ProjectTaskMemberDetails { get; set; }
        }
    }
}
