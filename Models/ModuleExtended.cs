﻿using DomainModelFramework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ReportingDomainModel
{
    [MetadataType(typeof(ModuleMetadata))]
    public partial class Module
    {
        internal class ModuleMetadata
        {
            [JsonIgnore]
            public virtual ICollection<ModulePrivilege> ModulePrivileges { get; set; }
        }

        public bool HasView
        {
            get
            {
                bool result = this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.VIEW.ToString()) != null;

                return result;
            }
        }

        public ModulePrivilege ViewModulePrivilege
        {
            get
            {
                if (this.HasView)
                {
                    return this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.VIEW.ToString());
                }

                return new ModulePrivilege();
            }
        }

        public bool HasInsert
        {
            get
            {
                bool result = this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.INSERT.ToString()) != null;

                return result;
            }
        }

        public ModulePrivilege InsertModulePrivilege
        {
            get
            {
                if (this.HasInsert)
                {
                    return this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.INSERT.ToString());
                }

                return new ModulePrivilege();
            }
        }

        public bool HasEdit
        {
            get
            {
                bool result = this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.EDIT.ToString()) != null;

                return result;
            }
        }

        public ModulePrivilege EditModulePrivilege
        {
            get
            {
                if (this.HasEdit)
                {
                    return this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.EDIT.ToString());
                }

                return new ModulePrivilege();
            }
        }

        public bool HasDelete
        {
            get
            {
                bool result = this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.DELETE.ToString()) != null;

                return result;
            }
        }

        public ModulePrivilege DeleteModulePrivilege
        {
            get
            {
                if (this.HasDelete)
                {
                    return this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.DELETE.ToString());
                }

                return new ModulePrivilege();
            }
        }


        public bool HasExport
        {
            get
            {
                bool result = this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.EXPORT.ToString()) != null;

                return result;
            }
        }

        public ModulePrivilege ExportModulePrivilege
        {
            get
            {
                if (this.HasExport)
                {
                    return this.ModulePrivileges.FirstOrDefault(x => x.Privilege.Name == PrivilegeName.EXPORT.ToString());
                }

                return new ModulePrivilege();
            }
        }

        public bool HasAdditional
        {
            get
            {
                bool result = this.ModulePrivileges.FirstOrDefault(x => x.Privilege.PrivilegeType.Name == PrivilegeTypeName.ADDITIONAL.ToString()) != null;

                return result;
            }
        }

        public IEnumerable<ModulePrivilege> AdditionalPrivileges
        {
            get
            {
                if (this.HasAdditional)
                {
                    return this.ModulePrivileges.Where(x => x.Privilege.PrivilegeType.Name == PrivilegeTypeName.ADDITIONAL.ToString());
                }

                return null;
            }
        }
    }
}
