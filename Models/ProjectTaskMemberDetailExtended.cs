﻿using DomainModelFramework;
using Newtonsoft.Json;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReportingDomainModel
{
    [MetadataType(typeof(ProjectTaskMemberDetailMetadata))]
    public partial class ProjectTaskMemberDetail
    {
        internal class ProjectTaskMemberDetailMetadata
        {
            [JsonIgnore]
            public virtual ProjectTaskMember ProjectTaskMember { get; set; }
        }

        public string DescriptionOrProjectTaskName
        {
            get
            {
                if (this.ProjectTask == null && string.IsNullOrEmpty(this.Description))
                    return "-";

                return string.IsNullOrEmpty(this.Description) ? this.ProjectTask.Name : this.Description;
            }
        }

        public string DescriptionOrNoDesc
        {
            get
            {
                return this.Description == null ? "No Desc" : this.Description;
            }
        }

        public string ProjectName { get; set; }
        public string ProjectTaskName { get; set; }
    }
}
