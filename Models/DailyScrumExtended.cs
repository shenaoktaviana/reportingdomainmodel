﻿using DomainModelFramework;
using ReportingResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReportingDomainModel
{
    [MetadataType(typeof(DailyScrumMetadata))]
    public partial class DailyScrum
    {
        internal class DailyScrumMetadata
        {

        }

        public string AssigneeName { get; set; }

        public string DateWithDay
        {
            get
            {
                return this.Date.ToString("ddd, dd MMM yyyy");
            }
        }
    }
}
