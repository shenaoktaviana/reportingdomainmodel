﻿using System.Text.RegularExpressions;

namespace ReportingDomainModel
{
    public class DMCommonUtilities
    {
        public static string TrimString(string text, int length)
        {
            if (!string.IsNullOrEmpty(text))
            {
                string trimmed = Regex.Replace(text, @"<[^>]+>", "");

                if (trimmed != null)
                {
                    if (trimmed.Length > length)
                    {
                        trimmed = trimmed.Substring(0, length) + "...";
                    }
                }
                return trimmed;
            }

            return string.Empty;
        }
    }
}
