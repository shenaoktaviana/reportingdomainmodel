//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportingDomainModel
{
    using System;
    using DomainModelFramework;using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    
    public partial class Area : BaseEntity
    {
    
     
    [Key()]
    
        public long AreaID { get; set; }
    
    
        public long CityID { get; set; }
    
    
        public string Name { get; set; }
    
    
        public bool IsActive { get; set; }
    
    
        
    
    
        
    
    
        
    
    
        
    
        public virtual City City { get; set; }
    }
}
