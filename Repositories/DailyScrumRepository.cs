﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ReportingDomainModel.Repositories
{
    public class DailyScrumRepository : DomainRepository<DailyScrum>
    {
        public void ManageState(DailyScrum model)
        {
            List<long> currentDetailsID = model.DailyScrumDetails.Select(x => x.DailyScrumDetailID).ToList();

            List<DailyScrumDetail> prevDetails = _context.Set<DailyScrumDetail>()
                .Where(x => x.DailyScrumID == model.DailyScrumID).ToList();

            foreach (DailyScrumDetail currentDetail in model.DailyScrumDetails.ToList())
            {
                DailyScrumDetail prevDetail = prevDetails
                    .Where(x => x.DailyScrumDetailID == currentDetail.DailyScrumDetailID)
                    .FirstOrDefault();

                if (prevDetail != null)
                    this._context.Entry(prevDetail).State = EntityState.Detached;

                if (currentDetail.DailyScrumDetailID > 0)
                    this._context.Entry(currentDetail).State = EntityState.Modified;
                else
                {
                    this._context.Entry(currentDetail).State = EntityState.Added;
                    currentDetail.CreatedBy = model.CreatedBy;
                    currentDetail.CreatedDate = model.CreatedDate;
                }

                currentDetail.ModifiedBy = model.ModifiedBy;
                currentDetail.ModifiedDate = model.ModifiedDate;
            }

            List<DailyScrumDetail> deletedDetails = prevDetails.Where(x => !currentDetailsID.Contains(x.DailyScrumDetailID)).ToList();

            foreach (DailyScrumDetail deletedDetail in deletedDetails)
            {
                this._context.Entry(deletedDetail).State = EntityState.Deleted;
            }
        }

        public void DeleteDetails(DailyScrum model)
        {
            foreach (DailyScrumDetail deletedDetail in model.DailyScrumDetails.ToList())
            {
                this._context.Entry(deletedDetail).State = EntityState.Deleted;
            }
        }
    }
}
