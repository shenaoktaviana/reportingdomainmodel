﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ReportingDomainModel.Repositories
{
    public class ProjectTaskMemberRepository : DomainRepository<ProjectTaskMember>
    {
        public void ManageState(ProjectTaskMember model)
        {
            List<long> currentDetailsID = model.ProjectTaskMemberDetails.Select(x => x.ProjectTaskMemberDetailID).ToList();

            List<ProjectTaskMemberDetail> prevDetails = _context.Set<ProjectTaskMemberDetail>()
                .Where(x => x.ProjectTaskMemberID == model.ProjectTaskMemberID).ToList();

            foreach (ProjectTaskMemberDetail currentDetail in model.ProjectTaskMemberDetails.ToList())
            {
                ProjectTaskMemberDetail prevDetail = prevDetails
                    .Where(x => x.ProjectTaskMemberDetailID == currentDetail.ProjectTaskMemberDetailID)
                    .FirstOrDefault();

                if (prevDetail != null)
                    this._context.Entry(prevDetail).State = EntityState.Detached;

                if (currentDetail.ProjectTaskMemberDetailID > 0)
                    this._context.Entry(currentDetail).State = EntityState.Modified;
                else
                {
                    this._context.Entry(currentDetail).State = EntityState.Added;
                    currentDetail.CreatedBy = model.CreatedBy;
                    currentDetail.CreatedDate = model.CreatedDate;
                }

                currentDetail.ModifiedBy = model.ModifiedBy;
                currentDetail.ModifiedDate = model.ModifiedDate;
            }

            List<ProjectTaskMemberDetail> deletedDetails = prevDetails.Where(x => !currentDetailsID.Contains(x.ProjectTaskMemberDetailID)).ToList();

            foreach (ProjectTaskMemberDetail deletedDetail in deletedDetails)
            {
                this._context.Entry(deletedDetail).State = EntityState.Deleted;
            }
        }

        public void DeleteDetails(ProjectTaskMember model)
        {
            foreach (ProjectTaskMemberDetail detail in model.ProjectTaskMemberDetails.ToList())
            {
                this._context.Entry(detail).State = EntityState.Deleted;
            }
        }
    }
}
