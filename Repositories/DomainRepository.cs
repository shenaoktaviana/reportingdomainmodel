﻿using DomainModelFramework;
using DomainModelFramework.Repositories;
using System;
using System.Data.Entity.Validation;
using System.Linq;

namespace ReportingDomainModel.Repositories
{
    public class DomainRepository<T> : BaseRepository<T> where T : BaseEntity
    {
        public DomainRepository()
        {
            this._context = new ReportingEntities();
        }

        public override int Save(long userId)
        {
            try
            {
                return (_context as ReportingEntities).SaveChanges(userId);
            }
            catch (DbEntityValidationException e)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = e.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                throw new Exception(fullErrorMessage);
            }
        }
    }
}
