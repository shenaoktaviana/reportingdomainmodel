﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ReportingDomainModel.Repositories
{
    public class UserRepository : DomainRepository<User>
    {
        public void UpdateUser(User model, long userID, bool changePassword, string password, bool updatePrivileges)
        {
            User prevUser = _context.Set<User>().FirstOrDefault(x => x.UserID == model.UserID);

            if (prevUser != null)
            {
                #region Update privileges
                if (updatePrivileges)
                {
                    if (prevUser.UserModulePrivileges == null)
                    {
                        if (model.UserModulePrivileges != null)
                        {
                            //insert new userModulePrivilege

                            foreach (UserModulePrivilege userModulePrivilege in model.UserModulePrivileges)
                            {

                                userModulePrivilege.CreatedBy = userID;
                                userModulePrivilege.ModifiedBy = userID;
                                userModulePrivilege.CreatedDate = DateTime.Now;
                                userModulePrivilege.ModifiedDate = DateTime.Now;
                            }

                            prevUser.UserModulePrivileges = model.UserModulePrivileges;

                            this._context.Entry(prevUser.UserModulePrivileges).State = EntityState.Added;
                        }
                    }
                    else
                    {
                        if (model.UserModulePrivileges == null || model.UserModulePrivileges.Count() == 0)
                        {
                            //delete userModulePrivilege
                            this._context.Entry(prevUser.UserModulePrivileges).State = EntityState.Deleted;
                        }
                        else
                        {
                            foreach (UserModulePrivilege userModulePrivilege in model.UserModulePrivileges)
                            {
                                UserModulePrivilege prevUserModulePrivilege = prevUser.UserModulePrivileges.FirstOrDefault(x => x.ModulePrivilegeID == userModulePrivilege.ModulePrivilegeID);

                                if (prevUserModulePrivilege != null)
                                {
                                    if (prevUserModulePrivilege.PrivilegeStateID != userModulePrivilege.PrivilegeStateID)
                                    {
                                        prevUserModulePrivilege.ModifiedBy = userID;
                                        prevUserModulePrivilege.ModifiedDate = DateTime.Now;
                                        prevUserModulePrivilege.PrivilegeStateID = userModulePrivilege.PrivilegeStateID;
                                        this._context.Entry(prevUserModulePrivilege).State = EntityState.Modified;
                                    }
                                }
                                else
                                {
                                    prevUserModulePrivilege = new UserModulePrivilege();
                                    prevUserModulePrivilege.ModifiedBy = userID;
                                    prevUserModulePrivilege.ModifiedDate = DateTime.Now;
                                    prevUserModulePrivilege.PrivilegeStateID = userModulePrivilege.PrivilegeStateID;
                                    prevUserModulePrivilege.CreatedBy = userID;
                                    prevUserModulePrivilege.CreatedDate = DateTime.Now;
                                    prevUserModulePrivilege.ModulePrivilegeID = userModulePrivilege.ModulePrivilegeID;
                                    prevUserModulePrivilege.UserID = userModulePrivilege.UserID;

                                    this._context.Entry(prevUserModulePrivilege).State = EntityState.Added;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Change password
                if (changePassword)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        prevUser.Password = password;
                    }
                }
                #endregion

                #region Update user attributes
                prevUser.ModifiedBy = userID;
                prevUser.ModifiedDate = DateTime.Now;
                prevUser.FirstName = model.FirstName;
                prevUser.LastName = model.LastName;
                prevUser.IsActive = model.IsActive;
                prevUser.Phone = model.Phone;

                this._context.Entry(prevUser).State = EntityState.Modified;
                #endregion
            }
        }

        public void DeleteModulePrivileges(List<UserModulePrivilege> userModelPrivileges)
        {
            foreach (UserModulePrivilege userModelPrivilege in userModelPrivileges)
            {
                this._context.Entry(userModelPrivilege).State = EntityState.Deleted;
            }
        }
    }
}
