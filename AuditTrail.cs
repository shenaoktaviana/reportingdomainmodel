//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportingDomainModel
{
    using System;
    using DomainModelFramework;using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    
    public partial class AuditTrail : BaseEntity
    {
    
     
    [Key()]
    
        public long AuditTrailID { get; set; }
    
    
        public string EntityType { get; set; }
    
    
        public string EntityID { get; set; }
    
    
        public string Action { get; set; }
    
    
        public string OldValue { get; set; }
    
    
        public string NewValue { get; set; }
    
    
        
    
    
        
    
    
        
    
    
        
    }
}
