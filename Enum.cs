﻿namespace ReportingDomainModel
{
    public enum ReportRowType
    {
        GroupingName,
        Content,
        GroupingFooter,
        TableFooter,
        SubgroupName,
        ParentGroupingName
    }

    public enum ReportRowAlign
    {
        Left,
        Right,
        Center
    }

    public enum ReportFontStyle
    {
        NORMAL,
        BOLD,
        ITALIC,
        GRAY,
        REDBOLD,
        RED
    }

    public enum ProjectTaskMemberStatus
    {
        WAITINGFORREVIEW,
        APPROVED,
        REJECTED
    }
}
