﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CulinaryBizDomainModel.Validators
{
    public class NumberRequiredAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            double numValue = 0.0;

            try
            {
                numValue = Convert.ToDouble(value);
            }
            catch (Exception)
            {
            }

            if (numValue == 0)
            {
                return new ValidationResult(ErrorMessage ?? String.Format(ErrorMessageString,
                    validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }
    }
}
