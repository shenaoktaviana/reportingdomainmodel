﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CulinaryBizDomainModel.Validators
{
    public class LanguageValidatorAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _languageProperty;
        public LanguageValidatorAttribute(string languageProperty)
        {
            _languageProperty = languageProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectType.GetProperty(_languageProperty);
            if (property == null)
            {
                return new ValidationResult(string.Format(
                    CultureInfo.CurrentCulture,
                    "Unknown property {0}",
                    new[] { _languageProperty }
                ));
            }
            var languagePropertyValue = property.GetValue(validationContext.ObjectInstance, null);

            if (languagePropertyValue != null || languagePropertyValue as Language != null)
            {
                if ((languagePropertyValue as Language).IsDefault)
                {
                    if (value == null || value as string == string.Empty)
                    {
                        return new ValidationResult(string.Format(
                            CultureInfo.CurrentCulture,
                            FormatErrorMessage(validationContext.DisplayName),
                            new[] { _languageProperty }
                        ));
                    }
                }
            }

            return ValidationResult.Success;
        }

        public IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "requiredif",
            };
            rule.ValidationParameters.Add("defaultlanguage", _languageProperty);
            yield return rule;
        }
    }
}
