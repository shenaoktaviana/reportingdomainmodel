﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportingDomainModel.Validators
{
    public class AlphaNumericRequiredAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string alphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            int alphaNumericCount = 0;

            try
            {
                string valueStr = value.ToString();
                alphaNumericCount = alphaNumeric.Where(x => valueStr.Contains(x.ToString())).Count();
            }
            catch (Exception)
            {
            }

            if (alphaNumericCount == 0)
            {
                return new ValidationResult(ErrorMessage ?? String.Format(ErrorMessageString,
                    validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }
    }
}
